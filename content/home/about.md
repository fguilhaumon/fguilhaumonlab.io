+++
# About/Biography widget.
widget = "about"
active = true
date = 2018-11-30T00:00:00

# Order that this section will appear in.
weight = 5

# List your academic interests.
[interests]
  interests = [
    "Ecology",
    "Conservation science",
    "Ecological analytics"
  ]

# List your qualifications (such as academic degrees).
[[education.courses]]
  course = "PhD in ecosystem science"
  institution = "University of Montpellier"
  year = 2010
 
+++

I'm a research scientist at [IRD](http://en.ird.fr/). I work at the [MARBEC](http://www.umr-marbec.fr/en/) lab, Montpellier, France. Currently, I'm located in la Réunion Island, France, [ENTROPIE](http://umr-entropie.ird.nc/index.php/home) lab. I also act as an associate editor for [Journal of Biogeography](https://onlinelibrary.wiley.com/journal/13652699)
<br/>
<br/>
My research applies theoretical and methodological advances in macroecology and biogeography to conservation biology. I’m particularly interested in discovering and documenting the distribution of different aspects of terrestrial and marine diversity and associated patterns of the human footprint. My goal is to integrate this knowledge at several spatial and temporal scales to help develop and deploy strategies for the conservation of biodiversity.

