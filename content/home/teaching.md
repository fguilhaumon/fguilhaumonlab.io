+++
# Custom widget.
# An example of using the custom widget to create your own homepage section.
# To create more sections, duplicate this file and edit the values below as desired.
widget = "custom"
active = true
date = 2018-11-20T00:00:00

# Note: a full width section format can be enabled by commenting out the `title` and `subtitle` with a `#`.
title = "Teaching"
subtitle = ""

# Order that this section will appear in.
weight = 60

+++

I am a teaching instructor for the following courses: 

[University of Mayotte](http://www.univ-mayotte.fr)

- SV305 Mathematics and satistics for ecology
- SV405 Data analyses in ecology

[University of Montpellier](https://www.umontpellier.fr/)

- HMBE383 Climate change impacts on marine ecosystems