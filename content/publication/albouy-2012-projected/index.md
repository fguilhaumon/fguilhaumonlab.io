+++
title = "Projected climate change and the changing biogeography of coastal Mediterranean fishes"
date = 2013-01-01
authors = ["Camille Albouy", "François Guilhaumon", "Fabien Leprieur", "Frida Ben Rais Lasram", "Samuel Somot", "Roland Aznar", "Laure Velez", "François Le Loc'h", "David Mouillot"]
publication_types = ["2"]
abstract = "Aim To forecast the potential effects of climate change in the Mediterranean Sea on the species richness and mean body size of coastal fish assemblages. Location The Mediterranean Sea. Methods Using an ensemble forecasting approach, we used species distribution modelling to project the potential distribution of 288 coastal fish species by the middle and end of the 21st century based on the IPCC A2 scenario implemented with the Mediterranean climatic model NEMOMED8. Results A mean rise of 1.4 °C was projected for the Mediterranean Sea by the middle of the 21st century and 2.8 °C by the end of the 21st century. Projections for the end of the century suggest that: (1) 54 species are expected to lose their climatically suitable habitat, (2) species richness was predicted to decrease across 70.4% of the continental shelf area, especially in the western Mediterranean Sea and several parts of the Aegean Sea, and (3) mean fish body size would increase over 74.8% of the continental shelf area. Small-bodied species that are not targeted by either commercial or recreational fleets presented, on average, the highest predicted decrease in geographic range size. Main conclusions Projected climate change in the Mediterranean Sea may have deleterious effects on coastal fish diversity, including a significant loss of climatically suitable habitat for endemic fish species. In addition, climate change may contribute to the loss of small and low trophic-level fishes, which may have ecosystem-wide impacts by reducing food supply to larger and higher trophic-level species. Fishing pressure is already selectively removing large-bodied species from marine ecosystems, and so fishing and climatic change might act in tandem to drive both direct and secondary extinctions."
selected = "false"
publication = "*Journal of Biogeography*"
tags = ["Body size", "Climate change", "Commercial fishing", "Mediterranean fish", "Recreational fishing", "Species distribution modelling"]
doi = "10.1111/jbi.12013"
+++

