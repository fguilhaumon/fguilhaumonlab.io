+++
title = "Functional biogeography of oceanic islands and the scaling of functional diversity in the Azores"
date = 2014-01-01
authors = ["R. J. Whittaker", "F. Rigal", "P. A. V. Borges", "P. Cardoso", "S. Terzopoulou", "F. Casanoves", "L. Pla", "F. Guilhaumon", "R. J. Ladle", "K. A. Triantis"]
publication_types = ["2"]
abstract = "Analyses of species-diversity patterns of remote islands have been crucial to the development of biogeographic theory, yet little is known about corresponding patterns in functional traits on islands and how, for example, they may be affected by the introduction of exotic species. We collated trait data for spiders and beetles and used a functional diversity index (FRic) to test for nonrandomness in the contribution of endemic, other native (also combined as indigenous), and exotic species to functional-trait space across the nine islands of the Azores. In general, for both taxa and for each distributional category, functional diversity increases with species richness, which, in turn scales with island area. Null simulations support the hypothesis that each distributional group contributes to functional diversity in proportion to their species richness. Exotic spiders have added novel trait space to a greater degree than have exotic beetles, likely indicating greater impact of the reduction of immigration filters and/or differential historical losses of indigenous species. Analyses of species occurring in native-forest remnants provide limited indications of the operation of habitat filtering of exotics for three islands, but only for beetles. Although the general linear (not saturating) pattern of trait-space increase with richness of exotics suggests an ongoing process of functional enrichment and accommodation, further work is urgently needed to determine how estimates of extinction debt of indigenous species should be adjusted in the light of these findings."
selected = "false"
publication = "*Proceedings of the National Academy of Sciences*"
tags = ["Arthropods", "Assembly rules", "Habitat destruction", "Island biogeography", "Saturation"]
url_pdf = "http://www.pnas.org/cgi/doi/10.1073/pnas.1218036111"
doi = "10.1073/pnas.1218036111"
+++

