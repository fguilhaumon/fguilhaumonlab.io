+++
title = "Global mismatch between species richness and vulnerability of reef fish assemblages"
date = 2014-01-01
authors = ["Valeriano Parravicini", "Sébastien Villéger", "Tim R. Mcclanahan", "Jesus Ernesto Arias-González", "David R. Bellwood", "Jonathan Belmaker", "Pascale Chabanet", "Sergio R. Floeter", "Alan M. Friedlander", "François Guilhaumon", "Laurent Vigliola", "Michel Kulbicki", "David Mouillot"]
publication_types = ["2"]
abstract = "The impact of anthropogenic activity on ecosystems has highlighted the need to move beyond the biogeographical delineation of species richness patterns to understanding the vulnerability of species assemblages, including the functional components that are linked to the processes they support. We developed a decision theory framework to quantitatively assess the global taxonomic and functional vulnerability of fish assemblages on tropical reefs using a combination of sensitivity to species loss, exposure to threats and extent of protection. Fish assemblages with high taxonomic and functional sensitivity are often exposed to threats but are largely missed by the global network of marine protected areas. We found that areas of high species richness spatially mismatch areas of high taxonomic and functional vulnerability. Nevertheless, there is strong spatial match between taxonomic and functional vulnerabilities suggesting a potential win-win conservation-ecosystem service strategy if more protection is set in these locations."
selected = "false"
publication = "*Ecology Letters*"
tags = ["Conservation", "Macroecology", "Risk assessment", "Sensitivity", "Vulnerability"]
doi = "10.1111/ele.12316"
+++

