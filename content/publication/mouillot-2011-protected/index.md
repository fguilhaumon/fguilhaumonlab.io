+++
title = "Protected and threatened components of fish biodiversity in the mediterranean sea"
date = 2011-01-01
authors = ["David Mouillot", "Camille Albouy", "Franois Guilhaumon", "Frida Ben Rais Lasram", "Marta Coll", "Vincent Devictor", "Christine N. Meynard", "Daniel Pauly", "Jean Antoine Tomasini", "Marc Troussellier", "Laure Velez", "Reg Watson", "Emmanuel J.P. Douzery", "Nicolas Mouquet"]
publication_types = ["2"]
abstract = "The Mediterranean Sea (0.82% of the global oceanic surface) holds 4%-18% of all known marine species (∼17,000), with a high proportion of endemism [1, 2]. This exceptional biodiversity is under severe threats [1] but benefits from a system of 100 marine protected areas (MPAs). Surprisingly, the spatial congruence of fish biodiversity hot spots with this MPA system and the areas of high fishing pressure has not been assessed. Moreover, evolutionary and functional breadth of species assemblages [3] has been largely overlooked in marine systems. Here we adopted a multifaceted approach to biodiversity by considering the species richness of total, endemic, and threatened coastal fish assemblages as well as their functional and phylogenetic diversity. We show that these fish biodiversity components are spatially mismatched. The MPA system covers a small surface of the Mediterranean (0.4%) and is spatially congruent with the hot spots of all taxonomic components of fish diversity. However, it misses hot spots of functional and phylogenetic diversity. In addition, hot spots of endemic species richness and phylogenetic diversity are spatially congruent with hot spots of fishery impact. Our results highlight that future conservation strategies and assessment efficiency of current reserve systems will need to be revisited after deconstructing the different components of biodiversity. textcopyright 2011 Elsevier Ltd."
selected = "false"
publication = "*Current Biology*"
doi = "10.1016/j.cub.2011.05.005"
+++

