+++
title = "Towards a consensus for calculating dendrogram-based functional diversity indices"
date = 2008-01-01
authors = ["Maud Mouchet", "François Guilhaumon", "Sébastien Villéger", "Norman W.H. Mason", "Jean Antoine Tomasini", "David Mouillot"]
publication_types = ["2"]
abstract = "The widely used FD index of functional diversity is based on the construction of a dendrogram. This index has been the subject of a strong debate concerning the choice of the distance and the clustering method to be used, since the method chosen may greatly affect the FD values obtained. Much of this debate has been centred around which method of dendrogram construction gives a faithful representation of species distribution in multidimensional functional trait space. From artificially generated datasets varying in species richness and correlations between traits, we test whether any single combination of clustering method(s) and distance consistently produces a dendrogram that most closely corresponds to the matrix of functional distances between pairs of species studied. We also test the ability of consensus trees, which incorporate features common to a range of different dendrograms, to summarize distance matrices. Our results show that no combination of clustering method(s) and distance constantly outperforms the others due to the complexity of interactions between correlations of traits, species richness, distance measures and clustering methods. Furthermore, the construction of a consensus tree from a range of dendrograms is often the best solution. Consequently, we recommend testing all combinations of distances and clustering methods (including consensus trees), then selecting the most reliable tree (with the lowest dissimilarity) to estimate FD value. Furthermore we suggest that any index that requires the construction of functional dendrograms potentially benefits from this new approach. Functio"
selected = "false"
publication = "*Oikos*"
doi = "10.1111/j.0030-1299.2008.16594.x"
+++

