+++
title = "Identifying the drivers of abundance and size of the invasive ctenophore Mnemiopsis leidyi in Northwestern Mediterranean lagoons"
date = 2016-01-01
authors = ["Floriane Delpy", "Séverine Albouy-Boyer", "Marc Pagano", "Delphine Thibault", "Jean Blanchot", "François Guilhaumon", "Juan Carlos Molinero", "Delphine Bonnet"]
publication_types = ["2"]
abstract = "Acknowledged as among the worst invasive marine species, Mnemiopsis leidyi has spread through European Seas since the mid-1980's. Here we report a bimonthly survey conducted in 2010-11 in three lagoons (Bages-Sigean, Thau and Berre) and at two adjacent coastal stations (Sète and SOMLIT-Marseille) along the French Mediterranean coast. M. leidyi was present only in Berre and Bages-Sigean with maximum abundances observed in late summer. M. leidyi adults were present year round in Berre with the largest organisms (̃6 cm) observed in April. In Bages-Sigean, they occurred in sufficient abundance to be recorded by fishermen between August and November. Multiple linear regressions highlighted that abundance in both lagoons was mainly influenced by direct effects of salinity and chlorophyll-a, and temperature to a lesser extent. While M. leidyi has not yet been recorded in Thau, the lagoon is continually monitored to detect the potential establishment of M. leidyi."
selected = "false"
publication = "*Marine Environmental Research*"
tags = ["Blooms", "Driving factors", "Gelatinous plankton", "Invasive species", "Mediterranean lagoons", "Mnemiopsis leidyi"]
doi = "10.1016/j.marenvres.2016.05.026"
+++

