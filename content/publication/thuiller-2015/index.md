+++
title = "Conserving the Functional and Phylogenetic Trees of Life of European Tetrapods"
date = 2015-01-01
authors = ["Wilfried Thuiller", "Luigi Maiorano", "Florent Mazel", "François Guilhaumon", "Gentile Francesco Ficetola", "Sébastien Lavergne", "Julien Renaud", "Cristina Roquet", "David Mouillot"]
publication_types = ["2"]
abstract = "Protected areas (PAs) are pivotal tools for biodiversity conservation on the Earth. Europe has had an extensive protection system since Natura 2000 areas were created in parallel with traditional parks and reserves. However, the extent to which this system covers not only taxonomic diversity but also other biodiversity facets, such as evolutionary history and functional diversity, has never been evaluated. Using high-resolution distribution data of all European tetrapods together with dated molecular phylogenies and detailed trait information, we first tested whether the existing European protection system effectively covers all species and in particular, those with the highest evolutionary or functional distinctiveness. We then tested the ability of PAs to protect the entire tetrapod phylogenetic and functional trees of life by mapping species' target achievements along the internal branches of these two trees. We found that the current system is adequately representative in terms of the evolutionary history of amphibians while it fails for the rest. However, the most functionally distinct species were better represented than they would be under random conservation efforts. These results imply better protection of the tetrapod functional tree of life, which could help to ensure long-term functioning of the ecosystem, potentially at the expense of conserving evolutionary history."
selected = "false"
publication = "*Philosophical Transactions of the Royal Society B: Biological Sciences*"
tags = ["Endemics", "Evolutionary distinctiveness", "Functional distinctiveness", "Gap analysis", "Protected areas"]
doi = "10.1098/rstb.2014.0005"
+++

