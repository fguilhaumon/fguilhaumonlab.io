+++
title = "Global mismatch between fishing dependency and larval supply from marine reserves"
date = 2017-01-01
authors = ["Marco Andrello", "François Guilhaumon", "Camille Albouy", "Valeriano Parravicini", "Joeri Scholtens", "Philippe Verley", "Manuel Barange", "U. Rashid Sumaila", "Stéphanie Manel", "David Mouillot"]
publication_types = ["2"]
abstract = "The effectiveness of marine reserves relies on whether they supply substantial recruitment outside their boundaries. Here, Andrello and colleagues use models of larval fish dispersal to show that countries most dependent on coastal fisheries receive the lowest larval supplies from marine reserves."
selected = "false"
publication = "*Nature Communications*"
doi = "10.1038/ncomms16039"
+++

