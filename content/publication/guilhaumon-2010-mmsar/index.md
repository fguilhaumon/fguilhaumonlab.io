+++
title = "mmSAR: an R-package for multimodel species–area relationship inference - Guilhaumon - 2010 - Ecography - Wiley Online Library"
date = 2010-01-01
authors = ["François Guilhaumon", "David Mouillot", "Olivier Gimenez"]
publication_types = ["2"]
abstract = "The species–area relationship (SAR) is one of the most fundamental tools in ecology. After almost a century of quantitative ecology, however, the quest for a “best SAR model” still remains elusive, with a substantial uncertainty about the best fitting SAR model frequently ..."
selected = "false"
publication = "*Ecography*"
url_pdf = "http://onlinelibrary.wiley.com.myaccess.library.utoronto.ca/doi/10.1111/j.1600-0587.2010.06304.x/full"
+++

