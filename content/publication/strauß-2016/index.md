+++
title = "Opposing Patterns of Seasonal Change in Functional and Phylogenetic Diversity of Tadpole Assemblages"
date = 2016-01-01
authors = ["Axel Strauß", "François Guilhaumon", "Roger Daniel Randrianiaina", "Katharina C. Wollenberg Valero", "Miguel Vences", "Julian Glos"]
publication_types = ["2"]
abstract = "Not Available"
selected = "false"
publication = "*PloS one*"
doi = "10.1371/journal.pone.0151744"
+++

