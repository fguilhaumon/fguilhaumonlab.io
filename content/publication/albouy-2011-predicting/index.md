+++
title = "Predicting trophic guild and diet overlap from functional traits: Statistics, opportunities and limitations for marine ecology"
date = 2011-01-01
authors = ["C. Albouy", "F. Guilhaumon", "S. Villéger", "M. Mouchet", "L. Mercier", "J. M. Culioli", "J. A. Tomasini", "F. Le Loc'h", "D. Mouillot"]
publication_types = ["2"]
abstract = "Fish diets provide information that can be used to explore and model complex ecosys- tems, and infer resource partitioning among species. The exhaustive sampling of prey items captured by each species remains, however, a demanding task. Therefore, predicting diets from other vari- ables, such as functional traits, may be a valuable method. Here, we attempted to predict trophic guild and diet overlap for 35 fish species using 13 ecomorphological traits related to feeding ecology. We compared linear discriminant analysis and random forest (RF) classifiers in their ability to predict trophic guild. We used generalized dissimilarity modelling to predict diet overlap from functional dis- tances between species pairs. All models were evaluated using the same cross-validation procedure. We found that fish trophic guilds were accurately predicted by an RF classifier, even with a limited number of traits, when no more than 7 guilds were defined. Prediction was no longer accurate when finer trophic guilds were created (8 or more guilds), whatever the combination of traits. Furthermore, predicting the degree of diet dissimilarity between species pairs, based on their ecomorphological traits dissimilarities, was profoundly unreliable (at least 76% of unexplained variation). These results suggest that we can predict fish trophic guilds accurately from ecomorphological traits, but not diet overlap and resource partitioning because of inherent versatility in fish diets. More generally, our sta- tistical framework may be applied to any kind of marine organism for which feeding strategies need to be determined from traits. KEY"
selected = "false"
publication = "*Marine Ecology Progress Series*"
tags = ["Fish", "Generalized dissimilarity modeling", "Mediterranean", "Non-linear model", "Random forest", "Versatility"]
doi = "10.3354/meps09240"
+++

