+++
title = "Arthropod diversity in a tropical forest"
date = 2012-01-01
authors = ["Yves Basset", "Lukas Cizek", "Philippe Cuénoud", "Raphael K. Didham", "François Guilhaumon", "Olivier Missa", "Vojtech Novotny", "Frode Ødegaard", "Tomas Roslin", "Jürgen Schmidl", "Alexey K. Tishechkin", "Neville N. Winchester", "David W. Roubik", "Henri Pierre Aberlenc", "Johannes Bail", "Héctor Barrios", "Jon R. Bridle", "Gabriela Castaño-Meneses", "Bruno Corbara", "Gianfranco Curletti", "Wesley Duarte Da Rocha", "Domir De Bakker", "Jacques H.C. Delabie", "Alain Dejean", "Laura L. Fagan", "Andreas Floren", "Roger L. Kitching", "Enrique Medianero", "Scott E. Miller", "Evandro Gama De Oliveira", "Jérôme Orivel", "Marc Pollet", "Mathieu Rapp", "Sérvio P. Ribeiro", "Yves Roisin", "Jesper B. Schmidt", "Line Sørensen", "Maurice Leponce"]
publication_types = ["2"]
abstract = "Most eukaryotic organisms are arthropods. Yet, their diversity in rich terrestrial ecosystems is still unknown. Here we produce tangible estimates of the total species richness of arthropods in a tropical rainforest. Using a comprehensive range of structured protocols, we sampled the phylogenetic breadth of arthropod taxa from the soil to the forest canopy in the San Lorenzo forest, Panama. We collected 6144 arthropod species from 0.48 hectare and extrapolated total species richness to larger areas on the basis of competing models. The whole 6000-hectare forest reserve most likely sustains 25,000 arthropod species. Notably, just 1 hectare of rainforest yields textgreater60% of the arthropod biodiversity held in the wider landscape. Models based on plant diversity fitted the accumulated species richness of both herbivore and nonherbivore taxa exceptionally well. This lends credence to global estimates of arthropod biodiversity developed from plant models."
selected = "false"
publication = "*Science*"
doi = "10.1126/science.1226727"
+++

