+++
title = "Estuarine , Coastal and Shelf Science Species e area relationships as a tool for the conservation of benthic invertebrates in Italian coastal lagoons"
date = 2012-01-01
authors = ["François Guilhaumon", "Alberto Basset", "Enrico Barbone", "David Mouillot"]
publication_types = ["2"]
abstract = ""
selected = "false"
publication = "*Estuarine, Coastal and Shelf Science*"
tags = ["conservation biology"]
url_pdf = "http://dx.doi.org/10.1016/j.ecss.2011.12.001"
doi = "10.1016/j.ecss.2011.12.001"
+++

