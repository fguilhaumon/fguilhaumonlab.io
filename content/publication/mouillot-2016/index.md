+++
title = "Global marine protected areas do not secure the evolutionary history of tropical corals and fishes"
date = 2016-01-01
authors = ["D. Mouillot", "V. Parravicini", "D. R. Bellwood", "F. Leprieur", "D. Huang", "P. F. Cowman", "C. Albouy", "T. P. Hughes", "W. Thuiller", "F. Guilhaumon"]
publication_types = ["2"]
abstract = "Although coral reefs support the largest concentrations of marine biodiversity worldwide, the extent to which the global system of marine-protected areas (MPAs) represents individual species and the breadth of evolutionary history across the Tree of Life has never been quantified. Here we show that only 5.7% of scleractinian coral species and 21.7% of labrid fish species reach the minimum protection target of 10% of their geographic ranges within MPAs. We also estimate that the current global MPA system secures only 1.7% of the Tree of Life for corals, and 17.6% for fishes. Regionally, the Atlantic and Eastern Pacific show the greatest deficit of protection for corals while for fishes this deficit is located primarily in the Western Indian Ocean and in the Central Pacific. Our results call for a global coordinated expansion of current conservation efforts to fully secure the Tree of Life on coral reefs."
selected = "false"
publication = "*Nature Communications*"
doi = "10.1038/ncomms10359"
+++

