+++
title = "Areas of climate stability of species ranges in the Brazilian cerrado: Disentangling uncertainties through time"
date = 2012-01-01
authors = ["Levi Carina Terribile", "Matheus Souza Lima-Ribeiro", "Miguel Bastos Araújo", "Nair Bizão", "Rosane Garcia Collevatti", "Ricardo Dobrovolski", "Amanda Assis Franco", "François Guilhaumon", "Jacqueline de Souza Lima", "Devanir Mitsuyuki Murakami", "João Carlos Nabout", "Guilherme de Oliveira", "Leciane Karita de Oliveira", "Suelen Gonçalves Rabelo", "Thiago Fernando Rangel", "Lorena Mendes Simon", "Thannya Nascimento Soares", "Mariana Pires de Campos Telles", "José Alexandre Felizola Diniz-Filho"]
publication_types = ["2"]
abstract = "Recognizing the location of climatically stable areas in the future is subjected to uncertainties from ecological niche models, climatic models, variation in species ranges responses, and from the climatic variation through time. Here, we proposed an approach based on hierarchical ANOVA to reduce uncertainties and to identify climatically stable areas, working with Cerrado tree species as a model organism. Ecological niche models were generated for 18 Cerrado tree species and their potential distributions were projected into past and future. Analyses of the sources of uncertainties in ensembles hindcasts/forecasts revealed that the time component was the most important source of variation, whereas the climatic models had the smallest effect. The species responses to climate changes do not showed marked differences within each time period. By comparing past and future predictions, a single continuous climatically stable area was identified, which should be considered as a potential improvement for spatial prioritization for conservation."
selected = "false"
publication = "*Natureza a Conservacao*"
tags = ["Climate change", "Climatically stable areas", "Ecological niche models", "Ensemble forecasting", "Systematic conservation planning"]
doi = "10.4322/natcon.2012.025"
+++

