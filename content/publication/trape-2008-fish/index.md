+++
title = "Fish predation by the water snake afronatrix anoscopus in a Guinean rainforest stream"
date = 2008-01-01
authors = ["Sébastien Trape", "François Guilhaumon", "Cellou Baldé"]
publication_types = ["2"]
abstract = "We collected fifty specimens of the colubrid water snake Afronatrix anoscopus during a single day in a 100 m section of a rainforest stream of southeastern Guinea. Food items were present in the digestive tracts of 17 specimens, with a significantly higher occurrence in females (48%) than in males (14%). Small fishes (textless2 g) belonging to four species (Tilapia brevimanus,Epiplatys hildegardae, Barbus sacratus, and Kribia kribensis) were the main prey, representing 89% of the total numbers of prey items. A anuscopus is probably one of the main predators of juvenile fishes in the West African rainforest."
selected = "false"
publication = "*Journal of Freshwater Ecology*"
doi = "10.1080/02705060.2008.9664233"
+++

