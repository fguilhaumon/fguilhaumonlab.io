+++
title = "Effects of the environment on fish juvenile growth in West African stressful estuaries"
date = 2009-01-01
authors = ["K. Diouf", "F. Guilhaumon", "C. Aliaume", "P. Ndiaye", "T. Do Chi", "J. Panfili"]
publication_types = ["2"]
abstract = "The knowledge of juvenile fish growth in extreme environmental conditions is a key to the understanding of adaptive responses and to the relevant management of natural populations. The juvenile growth of an extreme euryhaline tilapia species, Sarotherodon melanotheron (Cichlidae), was examined across a salinity gradient (20-118) in several West African estuarine ecosystems. Juveniles were collected during the reproduction period of two consecutive years (2003 and 2004) in six locations in the Saloum (Senegal) and Gambia estuaries. Age and growth were estimated using daily otolith microincrements. For each individual, otolith growth rates showed three different stages (slow, fast, decreasing): around 4 ± 0.5 $mu$m d-1during the first five days, 9 ± 0.5 $mu$m d-1during the next 15 days and 4 ± 0.50 $mu$m d-1at 60 days. Growth modelling and model comparisons were objectively made within an information theory framework using the multi-model inference from five growth models (linear, power, Gompertz, von Bertalanffy, and logistic). The combination of both the model adjustment inspection and the information theory model selection procedure allowed identification of the final set of models, including the less parameterised ones. The estimated growth rates were variable across spatial scales but not across temporal scales (except for one location), following exactly the salinity gradient with growth decrease towards the hypersaline conditions. The salinity gradient was closely related to all measured variables (condition factor, mean age, multi-model absolute growth rate) demonstrating the strong effect of hypersaline environmental conditions-induced by climate changes-on fish populations at an early stage. textcopyright 2009 Elsevier Ltd. All rights reserved."
selected = "false"
publication = "*Estuarine, Coastal and Shelf Science*"
tags = ["Gambian estuary", "Saloum estuary", "age estimation", "black-chinned tilapia", "growth models", "hypersalinity"]
doi = "10.1016/j.ecss.2009.02.031"
+++

