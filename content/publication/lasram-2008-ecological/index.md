+++
title = "Ecological correlates of dispersal success of Lessepsian fishes"
date = 2008-01-01
authors = ["F. Ben Rais Lasram", "J. A. Tomasini", "F. Guilhaumon", "M. S. Romdhane", "T. Do Chi", "D. Mouillot"]
publication_types = ["2"]
abstract = "Despite the importance of Lessepsian invasion by migrant fish species from the Red Sea into the Mediterranean Sea via the Suez Canal, determinants of invasive success have been poorly investigated. In this study, we reconstructed the spatio-temporal dynamics of all Lessepsian fish species in the Mediterranean Sea and analysed the relationship between ecological variables and dispersal rate. We created a database on species occurrences based on historical data (1869 to 2005) and estimated the dispersal rate of each species. Overall, 30 % of the Lessepsian species succeeded in colonizing the Mediterranean Sea. On average, the 43 Lessepsian species not included in the category 'absence of dispersal' disperse at a rate of 221 +/- 5.4 km yr(-1) (SE) on the northern side and 70 km yr(-1) (SE = 3 km yr(-1)) on the southern side. Among the ecological variables studied, climate match, the year of introduction and interactions of both factors were significantly correlated with dispersal success. According to our observations, subtropical species introduced before 1980 have an advantage in the dispersal process. The transition from the Levantine basin to the western basin is clearly associated with a deceleration in dispersal rate that is likely due to a thermal barrier. In addition, we showed that species with pelagic propagules (eggs) tend to disperse more on the northern side (in comparison to the southern side) than do species with benthic propagules. This pattern was related to the counterclockwise surface circulation in the Levantine basin. We concluded that crossing the Suez Canal does not guarantee successful invasion and widespread dispersal of fish populations and that species ecology is a key determinant for dispersal success."
selected = "false"
publication = "*Marine Ecology Progress Series*"
tags = ["Climate match", "Dispersal", "Exotic", "Lessepsian fish", "Mediterranean Sea", "Propagules", "Suez Canal", "Year of introduction"]
doi = "10.3354/meps07474"
+++

