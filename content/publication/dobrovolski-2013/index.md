+++
title = "Global agricultural expansion and carnivore conservation biogeography"
date = 2013-01-01
authors = ["Ricardo Dobrovolski", "Rafael D. Loyola", "François Guilhaumon", "Sidney F. Gouveia", "José Alexandre F. Diniz-Filho"]
publication_types = ["2"]
abstract = "Global conservation prioritization must address conflicting land uses. We tested for spatial congruence between agricultural expansion in the 21st century and priority areas for carnivore conservation worldwide. We evaluated how including agricultural expansion data in conservation planning reduces such congruence and estimated the consequences of such an approach for the performance of resulting priority area networks. We investigated the correlation between projections of agricultural expansion and the solutions of global spatial prioritizations for carnivore conservation through the implementation of different goals: (1) purely maximizing species representation and (2) representing species while avoiding sites under high pressure for agriculture expansion. We also evaluated the performance of conservation solutions based on species' representation and their spatial congruence with established global prioritization schemes. Priority areas for carnivore conservation were spatially correlated with future agricultural distribution and were more similar to global conservation schemes with high vulnerability. Incorporating future agricultural expansion in the site selection process substantially reduced spatial correlation with agriculture, resulting in a spatial solution more similar to global conservation schemes with low vulnerability. Accounting for agricultural expansion resulted in a lower representation of species, as the average proportion of the range represented reduced from 58% to 32%. We propose that priorities for carnivore conservation could be integrated into a strategy that concentrates different conservation actions towards areas where they are likely to be more effective regarding agricultural expansion. textcopyright 2013 Elsevier Ltd."
selected = "false"
publication = "*Biological Conservation*"
tags = ["Agriculture", "Global biodiversity conservation priorities", "IMAGE", "Mammal conservation", "Spatial prioritization", "Zonation"]
doi = "10.1016/j.biocon.2013.06.004"
+++

