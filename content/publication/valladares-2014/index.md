+++
title = "The effects of phenotypic plasticity and local adaptation on forecasts of species range shifts under climate change"
date = 2014-01-01
authors = ["Fernando Valladares", "Silvia Matesanz", "François Guilhaumon", "Miguel B. Araújo", "Luis Balaguer", "Marta Benito-Garzón", "Will Cornwell", "Ernesto Gianoli", "Mark van Kleunen", "Daniel E. Naya", "Adrienne B. Nicotra", "Hendrik Poorter", "Miguel A. Zavala"]
publication_types = ["2"]
abstract = "Species are the unit of analysis in many global change and conservation biology studies; however, species are not uniform entities but are composed of different, sometimes locally adapted, popula-tions differing in plasticity. We examined how intraspecific variation in thermal niches and pheno-typic plasticity will affect species distributions in a warming climate. We first developed a conceptual model linking plasticity and niche breadth, providing five alternative intraspecific sce-narios that are consistent with existing literature. Secondly, we used ecological niche-modeling techniques to quantify the impact of each intraspecific scenario on the distribution of a virtual species across a geographically realistic setting. Finally, we performed an analogous modeling exercise using real data on the climatic niches of different tree provenances. We show that when population differentiation is accounted for and dispersal is restricted, forecasts of species range shifts under climate change are even more pessimistic than those using the conventional assump-tion of homogeneously high plasticity across a species' range. Suitable population-level data are not available for most species so identifying general patterns of population differentiation could fill this gap. However, the literature review revealed contrasting patterns among species, urging greater levels of integration among empirical, modeling and theoretical research on intraspecific phenotypic variation."
selected = "false"
publication = "*Ecology Letters*"
tags = ["Climate change", "Climate variability hypothesis", "Ecological niche models", "Intraspecific variation", "Local adaptation", "Niche", "Phenotypic plasticity", "Population differentiation"]
doi = "10.1111/ele.12348"
+++

