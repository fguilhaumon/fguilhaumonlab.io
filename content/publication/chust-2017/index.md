+++
title = "Corrigendum: Mare Incognitum: A Glimpse into Future Plankton Diversity and Ecology Research"
date = 2017-01-01
authors = ["Guillem Chust", "Meike Vogt", "Fabio Benedetti", "Teofil Nakov", "Sébastien Villéger", "Anäis Aubert", "Sergio M. Vallina", "Damiano Righetti", "Fabrice Not", "Tristan Biard", "Lucie Bittner", "Anne-Sophie Benoiston", "Lionel Guidi", "Ernesto Villarino", "Charlie Gaborit", "Astrid Cornils", "Lucie Buttay", "Jean-Olivier Irisson", "Marlène Chiarello", "Alessandra L. Vallim", "Leocadio Blanco-Bercial", "Laura Basconi", "François Guilhaumon", "Sakina-Dorothée Ayata"]
publication_types = ["2"]
abstract = "With global climate change altering marine ecosystems, research on plankton ecology is likely to navigate uncharted seas. Yet, a staggering wealth of new plankton observations, integrated with recent advances in marine ecosystem modelling, may shed light on marine ecosystem structure and functioning. A EuroMarine foresight workshop on the “Impact of climate change on the distribution of plankton functional and phylogenetic diversity” (PlankDiv) identified five grand challenges for future plankton diversity and macroecology research: 1) What can we learn about plankton communities from the new wealth of high-throughput ‘omics' data? 2) What is the link between plankton diversity and ecosystem function? 3) How can species distribution models be adapted to represent plankton biogeography? 4) How will plankton biogeography be altered due to anthropogenic climate change? and 5) Can a new unifying theory of macroecology be developed based on plankton ecology studies? In this review, we discuss potential future avenues to address these questions, and challenges that need to be tackled along the way."
selected = "false"
publication = "*Frontiers in Marine Science*"
url_pdf = "http://journal.frontiersin.org/article/10.3389/fmars.2017.00122/full"
doi = "10.3389/fmars.2017.00122"
+++

