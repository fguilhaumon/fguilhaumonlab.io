+++
title = "Slow growth of the overexploited milk shark Rhizoprionodon acutus affects its sustainability in West Africa"
date = 2015-01-01
authors = ["A. Ba", "K. Diouf", "F. Guilhaumon", "J. Panfili"]
publication_types = ["2"]
abstract = "Age and growth of Rhizoprionodon acutus were estimated from vertebrae age bands. From December 2009 to November 2010, 423 R. acutus between 37 and 112 cm total length (LT ) were sampled along the Senegalese coast. Marginal increment ratio was used to check annual band deposition. Three growth models were adjusted to the length at age and compared using Akaike's information criterion. The Gompertz growth model with estimated size at birth appeared to be the best and resulted in growth parameters of L∞ = 139textperiodcentered55 (LT ) and K = 0textperiodcentered17 year(-1) for females and L∞ = 126textperiodcentered52 (LT ) and K = 0textperiodcentered18 year(-1) for males. The largest female and male examined were 8 and 9 years old, but the majority was between 1 and 3 years old. Ages at maturity estimated were 5textperiodcentered8 and 4textperiodcentered8 years for females and males, respectively. These results suggest that R. acutus is a slow-growing species, which render the species particularly vulnerable to heavy fishery exploitation. The growth parameters estimated in this study are crucial for stock assessments and for demographic analyses to evaluate the sustainability of commercial harvests."
selected = "false"
publication = "*Journal of Fish Biology*"
tags = ["Age", "Chondrichthyan", "Life-history traits", "Senegal", "Vertebrae"]
doi = "10.1111/jfb.12764"
+++

